import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { HomepageViewDataService } from '../../services/homepage-view-data.service';


@Component({
  selector: 'ws-homepage-passion',
  templateUrl: './homepage-passion.component.html',
  styleUrls: ['./homepage-passion.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomepagePassionComponent implements OnInit {

  constructor(private viewDataService: HomepageViewDataService) {
  }

  ngOnInit() {
  }

}
