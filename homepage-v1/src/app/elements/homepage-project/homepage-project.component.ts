import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { HomepageViewDataService } from '../../services/homepage-view-data.service';


@Component({
  selector: 'ws-homepage-project',
  templateUrl: './homepage-project.component.html',
  styleUrls: ['./homepage-project.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomepageProjectComponent implements OnInit {

  constructor(private viewDataService: HomepageViewDataService) {
  }

  ngOnInit() {
  }

}
