import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { HomepageViewDataService } from '../../services/homepage-view-data.service';


@Component({
  selector: 'ws-homepage-hobbies',
  templateUrl: './homepage-hobbies.component.html',
  styleUrls: ['./homepage-hobbies.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomepageHobbiesComponent implements OnInit {

  constructor(private viewDataService: HomepageViewDataService) {
  }

  ngOnInit() {
  }

}
