import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageBioComponent } from './homepage-bio.component';

describe('HomepageBioComponent', () => {
  let component: HomepageBioComponent;
  let fixture: ComponentFixture<HomepageBioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageBioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageBioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
