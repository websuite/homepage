import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageHobbyComponent } from './homepage-hobby.component';

describe('HomepageHobbyComponent', () => {
  let component: HomepageHobbyComponent;
  let fixture: ComponentFixture<HomepageHobbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageHobbyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageHobbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
