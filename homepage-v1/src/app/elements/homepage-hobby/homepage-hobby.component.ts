import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { HomepageViewDataService } from '../../services/homepage-view-data.service';


@Component({
  selector: 'ws-homepage-hobby',
  templateUrl: './homepage-hobby.component.html',
  styleUrls: ['./homepage-hobby.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomepageHobbyComponent implements OnInit {

  constructor(private viewDataService: HomepageViewDataService) {
  }

  ngOnInit() {
  }

}
