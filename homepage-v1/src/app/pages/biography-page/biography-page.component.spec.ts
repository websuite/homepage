import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiographyPageComponent } from './biography-page.component';

describe('BiographyPageComponent', () => {
  let component: BiographyPageComponent;
  let fixture: ComponentFixture<BiographyPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiographyPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiographyPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
