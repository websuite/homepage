import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ws-homepage-page',
  templateUrl: './homepage-page.component.html',
  styleUrls: ['./homepage-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomepagePageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
