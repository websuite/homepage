import { TestBed, inject } from '@angular/core/testing';

import { HomepageViewDataService } from './homepage-view-data.service';

describe('HomepageViewDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HomepageViewDataService]
    });
  });

  it('should be created', inject([HomepageViewDataService], (service: HomepageViewDataService) => {
    expect(service).toBeTruthy();
  }));
});
